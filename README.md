# Curso de Java da Loiane Groner

Transformar ***.java*** em ***.class*** para poder ser compilado

```
javac arquivo.java
```

Rodar arquivo .java

```
java arquivo.java
```
